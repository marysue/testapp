//
//  FavoritesViewController.h
//  002
//
//  Created by me on 10/26/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ExhibitorsViewController.h"

//@interface FavoritesViewController : UITableViewController <NSFetchedResultsControllerDelegate>
@interface FavoritesViewController : ExhibitorsViewController
@end
