//
//  FavoritesIconDelegate.h
//  002
//
//  Created by me on 10/27/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>

#define HOLD_STATIC_IMAGE(functionName, imagePath)\
static UIImage *functionName() {\
    static UIImage *image = nil;\
    if (nil == image) image = [UIImage imageNamed:@imagePath];\
    return image;\
}

@class Exhibitor;

typedef enum {
    FavoritesIconDelegateTagCell,
    FavoritesIconDelegateTagDetail
} FavoritesIconDelegateTag;

@protocol FavoritesIconDelegate <NSObject>
- (void)favoritesIconFor:(Exhibitor *)exhibitor wasTappedOn:(id)sender withTag:(FavoritesIconDelegateTag)tag;
@end
