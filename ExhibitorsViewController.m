//
//  ExhibitorsViewController.m
//  002
//
//  Created by me on 10/25/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "Exhibitor.h"
#import "DatabaseController.h"
#import "DetailViewController.h"
#import "ExhibitorsViewController.h"

@interface ExhibitorsViewController ()
@end

@implementation ExhibitorsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _databaseController = [DatabaseController instance];
    _managedObjectContext = _databaseController.managedObjectContext;
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)reloadData {
    [self.tableView reloadData];
}

#pragma mark - Table View
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_databaseController.databaseIsReady) return [[self.fetchedResultsController sections] count];
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!_databaseController.databaseIsReady) return 1;
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!_databaseController.databaseIsReady) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PlaceholderCell"];
        if (nil == cell) {            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"PlaceholderCell"];
        }
        
        cell.detailTextLabel.text = @"Loading...";
        cell.detailTextLabel.textAlignment = UITextAlignmentCenter;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    return [self makeCellForTableView:tableView atIndexPath:indexPath];
}

- (UITableViewCell *)makeCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    ExhibitorsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    Exhibitor *exhibitor = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.delegate = self;
    cell.exhibitor = exhibitor;
    cell.textLabel.text = exhibitor.name;
    cell.iconIsChecked = exhibitor.isInFavorites;
    return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    /* Don't show the abc index if table doesn't need to scroll. */
    if (tableView.contentSize.height <= tableView.frame.size.height) return nil;
    return [self.fetchedResultsController sectionIndexTitles];
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index {
    return [self.fetchedResultsController sectionForSectionIndexTitle:title atIndex:index];
}

# pragma mark - Segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showDetails"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Exhibitor *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
        id destinationViewController = [segue destinationViewController];
        [destinationViewController setDelegate:self];
        [destinationViewController setExhibitor:object];
    }
}

#pragma mark - Favorites icon delegate
- (void)favoritesIconFor:(Exhibitor *)exhibitor wasTappedOn:(id)sender withTag:(FavoritesIconDelegateTag)tag {
    BOOL shouldSave = NO;
    
    if (tag == FavoritesIconDelegateTagCell) {
        shouldSave = [self cellIconTapped:sender withExhibitor:exhibitor];
    } else if (tag == FavoritesIconDelegateTagDetail) {
        shouldSave = [self detailIconTapped:sender withExhibitor:exhibitor];
    }
    
    if (shouldSave) {
        NSError *error = nil;
        [exhibitor.managedObjectContext save:&error];
    }
}

- (BOOL)cellIconTapped:(ExhibitorsTableViewCell *)cell withExhibitor:(Exhibitor *)exhibitor {
    BOOL newStatus = !exhibitor.isInFavorites;
    exhibitor.isInFavorites = newStatus;
    cell.iconIsChecked = newStatus;
    return YES;
}

- (BOOL)detailIconTapped:(DetailViewController *)detailViewController withExhibitor:(Exhibitor *)exhibitor {
    BOOL newStatus = !exhibitor.isInFavorites;
    exhibitor.isInFavorites = newStatus;
    detailViewController.iconIsChecked = newStatus;
    [self.tableView reloadData];
    return YES;
}

#pragma mark - Fetched results controller
- (NSString *)cacheName {
    return @"Exhibitors";
}

- (NSFetchRequest *)fetchRequest {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Exhibitor" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortname" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];

    return fetchRequest;
}

- (NSFetchedResultsController *)fetchedResultsController {
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    self.managedObjectContext = _databaseController.managedObjectContext;
    [NSFetchedResultsController deleteCacheWithName:[self cacheName]];
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc]
                                                            initWithFetchRequest:[self fetchRequest]
                                                            managedObjectContext:self.managedObjectContext
                                                            sectionNameKeyPath:@"sortname"
                                                            cacheName:[self cacheName]];
    fetchedResultsController.delegate = self;
    self.fetchedResultsController = fetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    [ErrorHandler handleError:error fatal:YES];
	}
    
    return _fetchedResultsController;
}

@end
