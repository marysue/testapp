//
//  DatabaseController.h
//  002
//
//  Created by me on 10/26/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DatabaseController : NSObject
@property (nonatomic) BOOL databaseIsReady;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
+ (DatabaseController *)instance;
@end
