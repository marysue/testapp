//
//  Exhibitor.m
//  002
//
//  Created by me on 10/25/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "Exhibitor.h"
#import "ExhibitorDetails.h"

#define _isOk(object) ((object) != nil && (object) != [NSNull null])
#define eitherOr(object, defaultObject) ({ id e = (object); _isOk(e) ? e : (defaultObject); })
#define setString(object, key, dict) object.key = ({ id e = [dict objectForKey:@#key]; _isOk(e) ? [(NSString *)e trimmed] : @""; })
#define setArray(object, key, dict) object.key = eitherOr([dict objectForKey:@#key], [NSMutableArray array])
#define setDictionary(object, key, dict) object.key = eitherOr([dict objectForKey:@#key], [NSMutableDictionary dictionary])

@implementation Exhibitor

@dynamic name;
@dynamic isInFavorites;
@dynamic sortname;
@dynamic details;



// handle booths arrays
// some booths are like this: "A064,A060"
static NSArray *prepareBooths(NSArray *original) {
    static NSString *comma = @",";
    NSMutableArray *result = [NSMutableArray array];
    
    for (NSString *item in [original nonBlankTrimmedStrings]) {
        if ([item rangeOfString:comma].location != -1) {
            for (NSString *token in [item trimmedTokens:comma]) [result addObject:token];
            continue;
        }
        
        [result addObject:item];        
    }
    
    return result;
}

- (void)copyValuesFromJSONObject:(NSDictionary *)jsonObject {
    /* Header fields. */
    setString(self, name, jsonObject);
    setString(self, sortname, jsonObject);
    
    /* Create a details object. */
    ExhibitorDetails *details = [NSEntityDescription insertNewObjectForEntityForName:@"ExhibitorDetails" inManagedObjectContext:self.managedObjectContext];
    
    self.details = details;
    
    /* Collection fields. */
    setDictionary(details, address, jsonObject);
    setDictionary(details, listitems, jsonObject);
    
    /* String fields. */
    setString(details, email, jsonObject);
    setString(details, fair, jsonObject);
    setString(details, fax, jsonObject);
    setString(details, phone, jsonObject);
    setString(details, privateEmail, jsonObject);
    setString(details, subfair, jsonObject);
    setString(details, website, jsonObject);
    
    /* Fields whose names don't match the JSON object keys. */
    details.descriptionText = eitherOr([jsonObject objectForKey:@"description"], @"");
    
    /* Fields that need to be processed. */
    details.booths = prepareBooths(eitherOr([jsonObject objectForKey:@"booths"], @[]));
    
    details.objectid = ({
        NSNumber *number = [jsonObject objectForKey:@"id"];
        number ? [number integerValue] : -1;
    });
    
    /* Unknown object type. */
    details.logo = [jsonObject objectForKey:@"logo"];
}

@end
