//
//  NSArray+Extensions.h
//  002
//
//  Created by me on 10/28/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Extensions)
- (NSArray *)nonBlankTrimmedStrings;
@end
