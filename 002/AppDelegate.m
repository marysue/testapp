//
//  AppDelegate.m
//  002
//
//  Created by me on 10/25/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "Exhibitor.h"
#import "ExhibitorDetails.h"
#import "AppDelegate.h"
#import "DatabaseController.h"

@implementation AppDelegate {
    NSDictionary *_config;
    NSMutableData *_responseData;
    DatabaseController *_databaseController;
    BOOL _hadDBFile;
}

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    /* Load config. */
    _config = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Config" ofType:@"plist"]];
    _databaseController = [DatabaseController instance];
    
    _hadDBFile = [[NSFileManager defaultManager] fileExistsAtPath:[self persistentStoreURL].path];
    _databaseController.databaseIsReady = NO;
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:[self jsonSourceURL]];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    if (nil == connection) [ErrorHandler handleString:@"Unable to create URL connection" fatal:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [self saveContext];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext {
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"_02" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [self persistentStoreURL];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        [ErrorHandler handleError:error fatal:YES];
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory
// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Database management
- (NSURL *)persistentStoreURL {
    return [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"_02.sqlite"];
}

- (NSURL *)jsonSourceURL {
    //return [NSURL URLWithString:@"http://eventmatch.ecomobiel.nl/rest/exhibitors/ecomobiel"];
    //return [NSURL URLWithString:@"http://127.0.0.1:3000/exhibitors.json"];
    return [NSURL URLWithString:[_config objectForKey:@"jsonSourceURL"]];
}

- (void)dropDBContext {
    _persistentStoreCoordinator = nil;
    _managedObjectModel = nil;
    _managedObjectContext = nil;    
}

- (void)removePersistentStore {
    [self dropDBContext];
    [[NSFileManager defaultManager] removeItemAtURL:[self persistentStoreURL] error:nil];
}

- (void)iterateThroughDBWithBlock:(void(^)(Exhibitor *))block {
    NSError *error = nil;
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Exhibitor"
                                              inManagedObjectContext:self.managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:entity];
    
    NSArray *results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    if (error != nil) {
        [ErrorHandler handleError:error fatal:YES];
        return;
    }
    
    for (Exhibitor *item in results) {
        block(item);
    }    
}

- (NSIndexSet *)getFavorites {
    NSMutableIndexSet *favorites = [NSMutableIndexSet indexSet];
    [self iterateThroughDBWithBlock:^(Exhibitor *item) {
        if (item.isInFavorites && 0 < item.details.objectid) {
            [favorites addIndex:item.details.objectid];
        }
    }];
    return favorites;
}

- (void)applyFavorites:(NSIndexSet *)favorites {
    [self iterateThroughDBWithBlock:^(Exhibitor *item) {
        if ([favorites containsIndex:item.details.objectid]) {
            item.isInFavorites = YES;
        }
    }];
}

- (void)clearDatabase {
    [self iterateThroughDBWithBlock:^(Exhibitor *item) {
        [item.managedObjectContext deleteObject:item];
    }];
}

- (void)populateWithJSONData:(NSData *)data error:(NSError **)errorPtr {
    NSArray *items = [NSJSONSerialization JSONObjectWithData:data options:0 error:errorPtr];
    if (errorPtr && *errorPtr) return;
    NSIndexSet *favorites = nil;
    
    if (_hadDBFile) {
        favorites = [self getFavorites];
        //[self removePersistentStore];
        [self clearDatabase];
    }
    
    for (NSDictionary *dict in items) {
        [self insertJSONObject:dict];
    }
    
    if (favorites != nil) [self applyFavorites:favorites];
    
    [self.managedObjectContext save:errorPtr];
}

- (void)insertJSONObject:(NSDictionary *)jsonObject {
    Exhibitor *newExhibitor = [NSEntityDescription insertNewObjectForEntityForName:@"Exhibitor" inManagedObjectContext:self.managedObjectContext];
    [newExhibitor copyValuesFromJSONObject:jsonObject];
}

#pragma mark - Downloading
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    _responseData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_responseData appendData:data];  // append incoming data
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    // fatal error if there was no database file
    if (!_hadDBFile) {
        if ([error code] == kCFURLErrorNotConnectedToInternet) {
            // if we can identify the error, we can present a more precise message to the user.
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"No Connection Error"
                                                                 forKey:NSLocalizedDescriptionKey];
            NSError *noConnectionError = [NSError errorWithDomain:NSCocoaErrorDomain
                                                             code:kCFURLErrorNotConnectedToInternet
                                                         userInfo:userInfo];
            [ErrorHandler handleError:noConnectionError fatal:YES];
        } else {
            [ErrorHandler handleError:error fatal:YES];
        }
        
        return;
    }
    
    // otherwise just ignore silently and use the existing DB file
    _databaseController.databaseIsReady = YES;
    _databaseController.managedObjectContext = self.managedObjectContext;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self performSelectorOnMainThread:@selector(didLoadData:) withObject:nil waitUntilDone:NO];
}

- (void)didLoadData:(id)_ {
    NSError *error = nil;
    [self populateWithJSONData:_responseData error:&error];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    if (error != nil) { // something went wrong with the parsing
        [self removePersistentStore]; // in case it got corrupted
        [ErrorHandler handleError:error fatal:YES]; // there is no DB now
        return;
    }
    
    _databaseController.databaseIsReady = YES;
    _databaseController.managedObjectContext = self.managedObjectContext;
    id controller = [(UINavigationController*)self.window.rootViewController topViewController];
    if([controller respondsToSelector:@selector(reloadData)]) {
        [controller reloadData];
    }
}
@end
