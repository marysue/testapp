//
//  NSString+Extensions.h
//  002
//
//  Created by me on 10/27/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extensions)
- (BOOL)isBlank;
- (NSString *)trimmed;
- (NSArray *)trimmedTokens:(NSString *)delimeter;
- (NSString *)stringWithoutCharactersInSet:(NSCharacterSet *)set;
@end
