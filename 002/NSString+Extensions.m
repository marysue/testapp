//
//  NSString+Extensions.m
//  002
//
//  Created by me on 10/27/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "NSString+Extensions.h"

@implementation NSString (Extensions)
- (BOOL)isBlank {
    if (0 == [self length]) return YES;
    if (-1 == [self rangeOfCharacterFromSet:[[NSCharacterSet whitespaceAndNewlineCharacterSet] invertedSet]].location) return YES;
    return NO;
}

- (NSString *)trimmed {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (NSArray *)trimmedTokens:(NSString *)delimeter {
    NSMutableArray *result = [NSMutableArray array];
    NSArray *tokens = [self componentsSeparatedByString:delimeter];
    
    for (NSString *_token in tokens) {
        NSString *token = [_token trimmed];
        if (0 == token.length) continue;
        [result addObject:token];
    }
    return result;
}

- (NSString *)stringWithoutCharactersInSet:(NSCharacterSet *)set {
    return [[self componentsSeparatedByCharactersInSet:set] componentsJoinedByString:@""];
}
@end
