//
//  DetailViewController.m
//  002
//
//  Created by me on 10/25/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "DetailViewController.h"
#import "Exhibitor.h"
#import "ExhibitorDetails.h"

#define CELL_TAG_PHONE 0
#define CELL_TAG_FAX 1
#define CELL_TAG_WEBSITE 2
#define CELL_TAG_EMAIL 3

@interface ContactItem : NSObject
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *value;
@property (strong, nonatomic) NSString *urlString;
@property (nonatomic) NSInteger tag;
@end

@implementation ContactItem
@synthesize name, value, tag, urlString;
@end

@interface DetailViewController ()
{
    NSInteger _numberOfSections;
    NSArray *_exhibitorBooths;
    
    NSInteger _boothsSectionIndex;
    NSInteger _contactSectionIndex;
    NSInteger _placeholderSectionIndex;
    
    UIFont *_contactDetailCellFont;
    NSMutableArray *_contactItems;
}
- (void)configureView;
- (void)configureSections;
- (void)configureContactSection;
@end

@implementation DetailViewController

#pragma mark - Managing the detail item
@synthesize detailNameLabel, detailLogoImage, detailTextLabel;
@synthesize iconIsChecked = _iconIsChecked, delegate;

- (void)setExhibitor:(id)newExhibitor
{
    if (_exhibitor != newExhibitor) {
        _exhibitor = newExhibitor;
        [self configureView];
        [self configureSections];
    }
}

- (UIBarButtonItem *)systemAddButton {
    return [[UIBarButtonItem alloc]
     initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
     target:self
     action:@selector(toggleFavorites:)];
}

/* Avoid loading the same image more than once. */
HOLD_STATIC_IMAGE(_image_0, "images/button-unchecked.png")
HOLD_STATIC_IMAGE(_image_1, "images/button-checked.png")

- (UIBarButtonItem *)favoritesIconButton:(BOOL)checked {
    UIImage *image = checked ? _image_1() : _image_0();
    return [[UIBarButtonItem alloc]
     initWithImage:image
     style:UIBarButtonItemStylePlain
     target:self
     action:@selector(toggleFavorites:)];
}

- (void)configureView {
    // Update the user interface for the detail item.
    if (self.exhibitor) {
        if (self.tableHeaderView == nil) {
            [[NSBundle mainBundle] loadNibNamed:@"DetailHeaderView" owner:self options:nil];
            
            _iconIsChecked = self.exhibitor.isInFavorites;
            self.navigationItem.title = self.exhibitor.name;
            
            UIBarButtonItem *barButtonItem = [self favoritesIconButton:self.iconIsChecked];
            self.navigationItem.rightBarButtonItem = barButtonItem;
            
            self.detailNameLabel.text = self.exhibitor.name;
            self.detailTextLabel.text = self.exhibitor.details.descriptionText;
            
            _contactDetailCellFont = [UIFont fontWithName:@"Helvetica" size:14.0f];
            
            /* Reflow table header. */
            CGRect oldFrame = self.tableHeaderView.frame;
            CGRect newFrame = self.tableHeaderView.frame;
            
            /* Substract the initial height of the label. */
            newFrame.size.height -= self.detailTextLabel.frame.size.height;
            
            /* Resize the label to fit all text. */
            [self.detailTextLabel sizeToFit];
            
            /* Add the new height. */
            newFrame.size.height += self.detailTextLabel.frame.size.height;
            
            if (oldFrame.size.height < newFrame.size.height) {
                /* Resize the table header to fit the label. */
                self.tableHeaderView.frame = newFrame;
            }
            
            /* Embed the header into the table. */
            self.tableView.tableHeaderView = self.tableHeaderView;
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureView];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - UITableView Delegate/Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _numberOfSections;
}

- (void)configureSections {
    _numberOfSections = 0;
    
    _boothsSectionIndex = -1;
    _contactSectionIndex = -1;
    _placeholderSectionIndex = -1;
    
    _exhibitorBooths = self.exhibitor.details.booths;
    if (0 < [_exhibitorBooths count]) { //has booths section
        _boothsSectionIndex = _numberOfSections++;
    }
    
    [self configureContactSection];
    if (0 < [_contactItems count]) {
        _contactSectionIndex = _numberOfSections++;
    }
    
    if (0 == _numberOfSections) {
        _placeholderSectionIndex = _numberOfSections++;
    }
}

- (void)configureContactSection {
    _contactItems = [NSMutableArray array];
    ExhibitorDetails *details = self.exhibitor.details;
    
    if (![details.phone isBlank]) [self addField:details.phone toContactItems:_contactItems withTag:CELL_TAG_PHONE];
    if (![details.fax isBlank]) [self addField:details.fax toContactItems:_contactItems withTag:CELL_TAG_FAX];
    if (![details.website isBlank]) [self addField:details.website toContactItems:_contactItems withTag:CELL_TAG_WEBSITE];
    if (![details.email isBlank]) [self addField:details.email toContactItems:_contactItems withTag:CELL_TAG_EMAIL];
    
    //DLogExpr(_contactItems);
}

- (void)addField:(NSString *)field toContactItems:(NSMutableArray *)contactItems withTag:(NSInteger)tag {
    static NSCharacterSet *splitAt = nil;
    NSArray *tokens = nil;
    
    if (tag == CELL_TAG_PHONE || tag == CELL_TAG_FAX) {
        tokens = [field trimmedTokens:@","];
    } else {
        // sometimes website urls are separated by space
        if (nil == splitAt) {
            splitAt = [NSCharacterSet characterSetWithCharactersInString:@", \n\r\t\v\f"];
        }
        tokens = [[field componentsSeparatedByCharactersInSet:splitAt] nonBlankTrimmedStrings];
    }
    
    for (NSString *token in tokens) {
        ContactItem *item = [[ContactItem alloc] init];
        item.tag = tag;
        item.value = token;
        
        if (tag == CELL_TAG_PHONE) {
            item.name = @"Phone";
            NSString *number = [token stringWithoutCharactersInSet:[[NSCharacterSet decimalDigitCharacterSet] invertedSet]];
            item.urlString = [@"tel:" stringByAppendingString:number];
        } else if (tag == CELL_TAG_FAX) {
            item.name = @"Fax";
        } else if (tag == CELL_TAG_WEBSITE) {
            item.name = @"Website";
            if (0 == [token rangeOfString:@"http://"].location) item.urlString = token;
            else item.urlString = [@"http://" stringByAppendingString:token];
        } else if (tag == CELL_TAG_EMAIL) {
            item.name = @"Email";
            item.urlString = [@"mailto:" stringByAppendingString:token];
        }
        
        [_contactItems addObject:item];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *title = nil;
    
    if (section == _boothsSectionIndex) {
        title = @"Booths";
    } else if (section == _contactSectionIndex) {
        title = @"Contact details";
    } else if (section == _placeholderSectionIndex) {
        title = @"This item does not have enough data to display";
    }
    
    return title;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == _placeholderSectionIndex) return 150.0f;
    else return [tableView rowHeight];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rows = 0;
    
    if (section == _boothsSectionIndex) {
        rows = [_exhibitorBooths count];
    } else if (section == _contactSectionIndex) {
        rows = [_contactItems count];
    }
    
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = nil;
    
    if (indexPath.section == _boothsSectionIndex) {
        static NSString *cellIdentifier = @"BoothsCell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        cell.textLabel.text = [_exhibitorBooths objectAtIndex:indexPath.row];
    } else if (indexPath.section == _contactSectionIndex) {
        static NSString *cellIdentifier = @"ContactsCell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:cellIdentifier];
        }
        
        cell.detailTextLabel.font = _contactDetailCellFont;
        cell.detailTextLabel.lineBreakMode = UILineBreakModeWordWrap;
        cell.detailTextLabel.numberOfLines = 0;
        
        ContactItem *contactItem = [_contactItems objectAtIndex:indexPath.row];
        cell.tag = contactItem.tag;
        cell.textLabel.text = contactItem.name;
        cell.detailTextLabel.text = contactItem.value;
    } else {
        static NSString *cellIdentifier = @"GenericCell";
        
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        cell.textLabel.text = @"<Generic>";
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == _contactSectionIndex) {
        ContactItem *item = [_contactItems objectAtIndex:indexPath.row];
        
        /*DLog(@"cell height for %d: %f", indexPath.row, cell.frame.size.height);
        DLog(@"detail label width: %f", cell.detailTextLabel.frame.size.width);
        DLog(@"detail font: %@", cell.detailTextLabel.font);*/
        
        if (item.tag != CELL_TAG_FAX) {
            DLog(@"opening url \"%@\"", item.urlString);
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:item.urlString]];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == _contactSectionIndex) {
        NSString *cellText= ((ContactItem *)[_contactItems objectAtIndex:indexPath.row]).value;        
        CGSize constraintSize = CGSizeMake(207.0f, MAXFLOAT);
        CGSize labelSize = [cellText sizeWithFont:_contactDetailCellFont constrainedToSize:constraintSize lineBreakMode:UILineBreakModeWordWrap];
        return labelSize.height + 20.0f;
    }
    
    return 42.0f;
}

#pragma mark - Changing favorite status
- (void)toggleFavorites:(id)sender {
    [self.delegate favoritesIconFor:self.exhibitor wasTappedOn:self withTag:FavoritesIconDelegateTagDetail];
}

- (void)setIconIsChecked:(BOOL)iconIsChecked {
    _iconIsChecked = iconIsChecked;
    self.navigationItem.rightBarButtonItem.image = _iconIsChecked ? _image_1() : _image_0();
}
@end
