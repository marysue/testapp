//
//  DetailViewController.h
//  002
//
//  Created by me on 10/25/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavoritesIconDelegate.h"

@class Exhibitor;

@interface DetailViewController : UITableViewController
@property (strong, nonatomic) Exhibitor *exhibitor;
@property (weak, nonatomic) IBOutlet UILabel *detailNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *detailLogoImage;
@property (weak, nonatomic) IBOutlet UIView *tableHeaderView;
@property (weak, nonatomic) id <FavoritesIconDelegate> delegate;
@property (nonatomic) BOOL iconIsChecked;
@end
