//
//  NSArray+Extensions.m
//  002
//
//  Created by me on 10/28/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "NSArray+Extensions.h"

@implementation NSArray (Extensions)
- (NSArray *)nonBlankTrimmedStrings {
    NSMutableArray *result = [NSMutableArray array];
    
    for (id item in self) {
        if (![item isKindOfClass:[NSString class]]) continue;
        
        NSString *string = [(NSString *)item trimmed];
        if (0 < string.length) [result addObject:string];
    }
    
    return result;
}
@end
