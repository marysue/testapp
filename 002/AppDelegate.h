//
//  AppDelegate.h
//  002
//
//  Created by me on 10/25/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, NSURLConnectionDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, nonatomic) BOOL databaseReady;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
