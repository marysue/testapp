//
//  ErrorHandler.m
//  002
//
//  Created by me on 10/27/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "ErrorHandler.h"

@implementation ErrorHandler
+ (void)handleError:(NSError *)error fatal:(BOOL)fatal {
    [ErrorHandler handleString:error.localizedDescription fatal:fatal];
}

+ (void)handleString:(NSString *)string fatal:(BOOL)fatal {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:fatal ? @"Fatal error" : @"Error"
														message:string
													   delegate:self
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
    alertView.tag = fatal ? 1 : 0;
    [alertView show];
}

+ (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (1 == alertView.tag) exit(1); // fatal
}
@end
