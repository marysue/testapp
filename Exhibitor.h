//
//  Exhibitor.h
//  002
//
//  Created by me on 10/25/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class ExhibitorDetails;

@interface Exhibitor : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic) BOOL isInFavorites;
@property (nonatomic, retain) NSString * sortname;
@property (nonatomic, retain) ExhibitorDetails *details;

- (void)copyValuesFromJSONObject:(NSDictionary *)jsonObject;

@end
