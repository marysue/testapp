//
//  ErrorHandler.h
//  002
//
//  Created by me on 10/27/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorHandler : NSObject
+ (void)handleError:(NSError *)error fatal:(BOOL)fatal;
+ (void)handleString:(NSString *)string fatal:(BOOL)fatal;
@end
