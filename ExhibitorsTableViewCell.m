//
//  ExhibitorsTableViewCell.m
//  002
//
//  Created by me on 10/27/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "ExhibitorsTableViewCell.h"

@implementation ExhibitorsTableViewCell
@synthesize iconIsChecked = _iconIsChecked, delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self onLoad];
    }
    return self;
}

- (void)awakeFromNib {
    [self onLoad];
}

- (void)onLoad {
    _iconIsChecked = NO;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bannerTapped:)];
    tap.numberOfTapsRequired = 1;
    tap.numberOfTouchesRequired = 1;
    [self.imageView addGestureRecognizer:tap];
    self.imageView.userInteractionEnabled = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

/* Avoid loading the same image more than once. */
HOLD_STATIC_IMAGE(_image_0, "images/listitem-unchecked.png")
HOLD_STATIC_IMAGE(_image_1, "images/listitem-checked.png")

- (void)setIconIsChecked:(BOOL)iconIsChecked {
    _iconIsChecked = iconIsChecked;
    self.imageView.image = _iconIsChecked ? _image_1() : _image_0();
}

- (void)bannerTapped:(UIGestureRecognizer *)gestureRecognizer {
    [self.delegate favoritesIconFor:self.exhibitor wasTappedOn:self withTag:FavoritesIconDelegateTagCell];
}

@end
