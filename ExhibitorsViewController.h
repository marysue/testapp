//
//  ExhibitorsViewController.h
//  002
//
//  Created by me on 10/25/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "FavoritesIconDelegate.h"
#import "ExhibitorsTableViewCell.h"

@class DatabaseController;

@interface ExhibitorsViewController : UITableViewController <NSFetchedResultsControllerDelegate, FavoritesIconDelegate>
@property (weak, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) DatabaseController *databaseController;

/* Overridable methods. */

// this method gets called only when the database is ready
- (UITableViewCell *)makeCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath;
- (void)reloadData;
- (NSString *)cacheName;
- (NSFetchRequest *)fetchRequest;
@end
