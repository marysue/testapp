//
//  ExhibitorDetails.h
//  002
//
//  Created by me on 10/25/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Exhibitor;

@interface ExhibitorDetails : NSManagedObject

@property (nonatomic) int32_t objectid;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * privateEmail;
@property (nonatomic, retain) NSString * fax;
@property (nonatomic, retain) NSString * fair;
@property (nonatomic, retain) id booths;
@property (nonatomic, retain) id listitems;
@property (nonatomic, retain) id logo;
@property (nonatomic, retain) id address;
@property (nonatomic, retain) NSString * descriptionText;
@property (nonatomic, retain) NSString * subfair;
@property (nonatomic, retain) NSString * website;
@property (nonatomic, retain) Exhibitor *header;

@end
