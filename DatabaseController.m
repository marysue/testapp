//
//  DatabaseController.m
//  002
//
//  Created by me on 10/26/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "DatabaseController.h"

@implementation DatabaseController
@synthesize databaseIsReady, managedObjectContext;

+ (DatabaseController *)instance {
    static DatabaseController *instance = nil;
    if (nil == instance) instance = [[DatabaseController alloc] init];
    return instance;
}

@end
