//
//  ExhibitorDetails.m
//  002
//
//  Created by me on 10/25/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "ExhibitorDetails.h"
#import "Exhibitor.h"


@implementation ExhibitorDetails

@dynamic objectid;
@dynamic phone;
@dynamic email;
@dynamic privateEmail;
@dynamic fax;
@dynamic fair;
@dynamic booths;
@dynamic listitems;
@dynamic logo;
@dynamic address;
@dynamic descriptionText;
@dynamic subfair;
@dynamic website;
@dynamic header;

@end
