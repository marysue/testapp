//
//  FavoritesViewController.m
//  002
//
//  Created by me on 10/26/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import "Exhibitor.h"
#import "DatabaseController.h"
#import "DetailViewController.h"
#import "FavoritesViewController.h"

@interface FavoritesViewController ()
@end

@implementation FavoritesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureView];
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)reloadData {
    [super reloadData];
    [self configureView];
}

- (void)configureView {
    if (self.databaseController.databaseIsReady) {
        self.navigationItem.rightBarButtonItem = self.editButtonItem;
    }    
}

#pragma mark - Table view data source
- (UITableViewCell *)makeCellForTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    Exhibitor *exhibitor = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = exhibitor.name;
    return cell;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.databaseController.databaseIsReady) return;
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        Exhibitor *exhibitor = [self.fetchedResultsController objectAtIndexPath:indexPath];
        exhibitor.isInFavorites = NO;
        [exhibitor.managedObjectContext save:nil];
        [self.fetchedResultsController performFetch:nil];
        //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [tableView reloadData];
    }  
}

#pragma mark - Fetched results controller
- (NSString *)cacheName {
    return @"Favorites";
}

- (NSFetchRequest *)fetchRequest {
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];

    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Exhibitor" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    [fetchRequest setFetchBatchSize:20];
    
    // Fetch objects that match predicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isInFavorites == YES"];
    [fetchRequest setPredicate:predicate];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sortname" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptor]];
    
    return fetchRequest;
}

@end
