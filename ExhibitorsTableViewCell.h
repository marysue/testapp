//
//  ExhibitorsTableViewCell.h
//  002
//
//  Created by me on 10/27/12.
//  Copyright (c) 2012 edu.self. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavoritesIconDelegate.h"

@class Exhibitor;

@interface ExhibitorsTableViewCell : UITableViewCell
@property (nonatomic) BOOL iconIsChecked;
@property (nonatomic, strong) Exhibitor *exhibitor;
@property (nonatomic, weak) id <FavoritesIconDelegate> delegate;
@end
